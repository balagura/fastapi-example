from fastapi import FastAPI, Query
from collections import defaultdict
import os
import re
import pandas as pd
from typing import Annotated

files = {}
for f in os.listdir('lumi_data'):
    m = re.match(r'(.*)_v([0-9]+)\.csv\.gz', f).groups()
    if len(m) == 2:
        version = int(m[1])
        if m[0] in files:
            files[m[0]].append(version)
        else:
            files[m[0]] = [version]

for k,v in files.items():
    v.sort() 

# Make a structure: runs_all_versions[runNumber][analysis_version] -> namedtuple with lumi info:
runs_all_versions = defaultdict(lambda: defaultdict(dict))
# only the latest version:
runs = {}
# Note: run can be present in a previous version (eg. v1) and then removed as
# bad in the subsequent analysis (v2). Then, it has only version v1, BUT DO
# NOT CONSIDER IT AS THE LATEST VERSION: according to v2 it should be EXCLUDED
# from physics analysis. The information on the latest version is lost after
# merging all files + versions, it should be taken from the FILE NAMES with
# the format <file_base>_vN.csv.gz. One should take the latest lumi only from
# the files with the largest version number N in their names, and only for the
# runs present there.
#
# For avoiding confusion, if any version is missing for the run present in
# runs_all_versions[] structure (ie. when this run appears in one of
# <file_base>_vN.csv.gz, N=1,... but not in all), the corresponding
# runs_all_versions[run][version] is filled with None.
#
# It is also checked that the run appears only in one series of files
# <file_base>_vN.csv.gz, ie. the correspondance run -> <file_base> (and so run
# -> <file_base> -> the latest version) is unique.
#
file_with_run = {}
for file_base,versions in files.items():
    max_version = max(versions)
    for version in versions:
        latest_version = (version == max_version)
        df = pd.read_csv('lumi_data/' + file_base + '_v' + str(version) + '.csv.gz')
        # pandas DataFrame will be converted to a dict {run: namedtuples}.
        # The namedtuple names should be python identifiers not starting with underscore, otherwise
        # the DataFrame names will be converted to just "_n" and lost. So, make valid namedtuple names
        # from the DataFrame names first:
        df.rename(lambda s: re.sub('^[_0-9]+', '', # remove underscore and digits in front
                                   # and substitute non-characters/digits/underscore by underscores:
                                   re.sub('\W','_', s)), axis='columns', inplace = True)
        if any(name not in df.columns for name in ('run', 'n_lumi_events',
                                                   'lumi', 'lumi_error')):
            raise KeyError("one of 'run', 'n_lumi_events', 'lumi', 'lumi_error' fields not found")
        if df.dtypes['run'] != 'int64':
            raise ValueError(f'not integer run number found')
        for row in df.itertuples(index = False):
            row = row._asdict()
            run = int(row.pop('run'))
            runs_all_versions[run][version] = row
            if latest_version:
                runs[run] = row
            if run in file_with_run.keys():
                if file_with_run[run] != file_base:
                    raise KeyError(f"run {run} is present both in file_base_v" +
                                   version + " and in {file_with_run[run]}")
            else:
                file_with_run[run] = file_base

for run,data in runs_all_versions.items():
    file_base = file_with_run[run]
    versions = files[file_base]
    for version in versions:
        if version not in data.keys():
            data[version] = None
            
app = FastAPI()

@app.get("/")
def print_help():
    return {"Help": "append to URL run/<run_number> to get luminosity for <run_number>, " +
            "append docs or redoc for more options"}

@app.get("/run/{run}")
def get_lumi_data_per_run(run: int, add: Annotated[list[int], Query()] = []):
    return {r: runs.get(r) for r in set([run] + add)}

@app.get("/run/{run}/all_versions")
def get_lumi_data_per_run_for_all_analysis_versions(run: int):
    return runs_all_versions[run]

@app.get("/all_runs")
def get_lumi_data_for_all_runs():
    return runs

@app.get("/all_runs/all_versions")
def get_lumi_data_for_all_runs_and_versions():
    return runs_all_versions

@app.get("/run_list")
def get_only_run_list():
    return [r for r in runs.keys()]

@app.get("/run_list/all_versions")
def get_only_run_list_for_all_versions():
    return {run: [v for v in versions.keys()] for run,versions in runs_all_versions.items()}
